//
//  ViewController.swift
//  TConverter
//
//  Created by roman on 5/28/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var celsiousLabel: UILabel!
    @IBOutlet weak var fahrenheitLabel: UILabel!
    @IBOutlet weak var slider: UISlider! {
        didSet {
            slider.maximumValue = 100
            slider.minimumValue = 0
            slider.value = 0
        }
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        let temperatureCalsius = Int(round(sender.value))
        celsiousLabel.text = "\(temperatureCalsius)ºC"
        
        let fahrenheitTemperature = Int(round((sender.value * 9 / 5) + 32))
        fahrenheitLabel.text = "\(fahrenheitTemperature)ºF"
    }
    
}

